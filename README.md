# Application Simple Next.js

Ce projet est une template simple de base pour les prochains projets et pour les démos clients, c'est une template front end avec reactJS, NextJS et Material UI pour les composons interface, le template contient une seule page accueil, avec un menu de profil d'utilisateur ainsi que c'est une template multilinguisme.

## Installation et exécution locale :

```sh
$ npm install
$ npm run dev
```

ouvrir [http://localhost:3000](http://localhost:3000) dans le navigateur pour voir le projet.

### Autres commandes

-   Exécuter [Storybook](https://storybook.js.org) : `npm run storybook`.

## Rappel : outils de développement

-   Node.js v14.17.6
-   npm v7.22.0
-   Git
-   material-ui
-   next v11.1.2
-   react v17.0.2
-   VSCode avec les plugins :
    -   `ESLint` pour l'analyse de code
    -   `Prettier - Code formatter` pour la mise en forme Javascript et md.
-   husky v4.3.0
-   commitlint
-   ESLint
-   Prettier
-   storyBook
-   i18next
-   axios
-   ramda

## Structure du projet

```
.
├── README.md
├── .storybook
├── components
├── config
├── pages
├── public
    ├── static
    ├── locales
├── stories
└── theme
```

-   `components` contient les composants de l'interface utilisateur.
-   `config` contient les fichiers et procédures de configutation des différents modules, dont la barre de navigation.
-   `pages` contient les pages de l'application (en React selon le framework Next)
-   `public` contient les fichiers statiques, dont
    -   `static` contient les images et le favicon.
    -   `locales` contient les fichiers de traduction.
-   `stories` contient les storybook du projet.
-   `theme` contient le JS de définition du thème (cf. [doc material-ui](https://material-ui.com/customization/themes/))
