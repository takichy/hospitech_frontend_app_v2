/**
 * Contexte de l'application
 */

// React
import React, { createContext, useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';

// Material UI
import Box from '@material-ui/core/Box';
import CircularProgress from '@material-ui/core/CircularProgress';

// Contexte de l'application
const AppContext = createContext();

// Contexte par défaut
const CONTEXT_STORAGE_KEY = 'ccr-context';
const DEFAULT_CONTEXT = {};

// Composant fournisseur
function AppContextProvider({ contextFactories, children }) {
    const [ssrDone, setSsrDone] = useState(false);
    const persistedState = typeof window !== 'undefined' ? window.sessionStorage.getItem(CONTEXT_STORAGE_KEY) : null;
    const defaultState = persistedState ? JSON.parse(persistedState) : DEFAULT_CONTEXT;

    const subContexts = contextFactories.map(contextFactory => contextFactory(defaultState));

    // Opération de réinitialisation
    function reset() {
        subContexts.forEach(subContext => subContext.reset && subContext.reset());
    }

    // Contexte initial
    const initialContext = subContexts.reduce(
        (context, subContext) => ({ ...context, [subContext.key]: subContext.state, ...subContext.operations, reset }),
        {}
    );

    // Log initialisation
    useEffect(() => {
        console.info('[context-provider] contexte initial:', initialContext);
    }, []);

    // Sauvegarde
    const contextDependencies = subContexts.map(subContext => subContext.state);

    function saveContext() {
        const context = subContexts.reduce((context, subContext) => ({ ...context, [subContext.key]: subContext.state }), {});
        window.sessionStorage.setItem(CONTEXT_STORAGE_KEY, JSON.stringify(context));
    }

    useEffect(() => {
        saveContext();
    }, contextDependencies);

    // Tracking de l'état "server side"
    useEffect(() => {
        setSsrDone(true);
    }, []);

    // Rendu des composants
    return !ssrDone ? (
        <Box width="100vw" height="100vh" display="flex" justifyContent="center" alignItems="center">
            <CircularProgress color="primary" size={128} />
        </Box>
    ) : (
        <AppContext.Provider value={initialContext}>{children}</AppContext.Provider>
    );
}

AppContextProvider.propTypes = {
    children: PropTypes.node.isRequired,
    contextFactories: PropTypes.arrayOf(PropTypes.func).isRequired
};

// Utilisation du contexte
function useApplicationContext(filter = context => context) {
    const context = useContext(AppContext);
    // Retourne éventuellement un sous-ensemble
    return filter(context);
}

// On n'exporte que l'usage du contexte et le fournisseur
// AppContext n'est exporté que pour les tests
export { useApplicationContext, AppContextProvider, AppContext };
