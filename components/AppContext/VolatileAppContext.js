/**
 * Contexte de l'application volatile (non persistant)
 */

// React
import React, { createContext, useContext } from 'react';
import PropTypes from 'prop-types';

// Contexte de l'application
const VolatileAppContext = createContext();

// Contexte par défaut
const DEFAULT_CONTEXT = {};

// Composant fournisseur
function VolatileAppContextProvider({ contextFactories, children }) {
    const subContexts = contextFactories.map(contextFactory => contextFactory(DEFAULT_CONTEXT));

    // Opération de réinitialisation
    function reset() {
        subContexts.forEach(subContext => subContext.reset && subContext.reset());
    }

    // Contexte initial
    const initialContext = subContexts.reduce(
        (context, subContext) => ({ ...context, [subContext.key]: subContext.state, ...subContext.operations, reset }),
        {}
    );

    // Rendu des composants
    return <VolatileAppContext.Provider value={initialContext}>{children}</VolatileAppContext.Provider>;
}

VolatileAppContextProvider.propTypes = {
    children: PropTypes.node.isRequired,
    contextFactories: PropTypes.arrayOf(PropTypes.func).isRequired
};

// Utilisation du contexte
function useVolatileApplicationContext(filter = context => context) {
    const context = useContext(VolatileAppContext);
    // Retourne éventuellement un sous-ensemble
    return filter(context);
}

// On n'exporte que l'usage du contexte et le fournisseur
// AppContext n'est exporté que pour les tests
export { useVolatileApplicationContext, VolatileAppContextProvider, VolatileAppContext };
