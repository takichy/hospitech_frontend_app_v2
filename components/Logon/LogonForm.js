/*
 * LogonForm.js
 * Formulaire de logon.
 */

// Nextjs
import Link from 'next/link';

// Material
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import MuiLink from '@material-ui/core/Link';

// Formulaire
import { Formik, Field, Form } from 'formik';
import * as yup from 'yup';
import TextField from '../formik/TextField';

// Styles
import { makeStyles } from '@material-ui/styles';
import userController from '../../controllers/user/user-controller';
const useStyles = makeStyles(theme => ({
    form: {
        width: '30%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        backgroundColor: 'white',
        padding: '35px'
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    },
    error: {
        backgroundColor: 'red'
    }
}));

// Bloc des liens perte de mot de passe ou enregistrement
function ForgotOrSignup() {
    return (
        <Grid container justifyContent="space-between">
            <Link href="/forgot_password">
                <MuiLink variant="body2" style={{ cursor: 'pointer' }}>
                    Mot de passe oublié ?
                </MuiLink>
            </Link>
            <Link href="/subscribe">
                <MuiLink variant="body2" style={{ cursor: 'pointer' }}>
                    S'inscrire
                </MuiLink>
            </Link>
        </Grid>
    );
}

// Composant principal
export default function LogonForm() {
    const classes = useStyles();
    const controller = userController();

    // Schéma de validation du formulaire
    const formSchema = yup.object().shape({
        email: yup.string().email('Veuiller entrer un email valide').required('Un email est obligatoire'),
        password: yup
            .string()
            .required('Un mot de passe est obligatoire')
            .matches(
                /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
                'Doit contenir 8 caractères, une majuscule, une minuscule, un chiffre et un caractère spécial'
            )
    });

    return (
        <Formik
            initialValues={{ email: '', password: '' }}
            validationSchema={formSchema}
            validateOnMount
            onSubmit={values => controller.handleLogin(values)}
        >
            {({ isSubmitting, isValid, errors, touched }) => (
                <Form className={classes.form}>
                    <Field
                        type="email"
                        name="email"
                        label="Adresse email"
                        component={TextField}
                        margin="normal"
                        required
                        fullWidth
                        autoComplete="email"
                        autoFocus
                        error={errors.email && touched.email}
                        helperText={errors.email && touched.email ? errors.email : ''}
                        data-testid="email"
                    />
                    <Field
                        type="password"
                        name="password"
                        label="Mot de passe"
                        component={TextField}
                        margin="normal"
                        required
                        fullWidth
                        autoFocus
                        error={errors.password && touched.password}
                        helperText={errors.password && touched.password ? errors.password : ''}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        disabled={isSubmitting || !isValid}
                        className={classes.submit}
                        data-testid="submit"
                    >
                        SE CONNECTER
                    </Button>
                    <ForgotOrSignup />
                </Form>
            )}
        </Formik>
    );
}
