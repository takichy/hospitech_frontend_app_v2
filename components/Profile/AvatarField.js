/**
 * Composant de "saisie" d'un avatar
 */

// React
import PropTypes from 'prop-types';

// Material-UI
import Grid from '@material-ui/core/Grid';
import Slider from '@material-ui/core/Slider';
import Typography from '@material-ui/core/Typography';

// Autres composants
import AvatarEditor from 'react-avatar-editor';
import Dropzone from 'react-dropzone';

// Controller
// import { useAvatarFieldController } from './AvatarField.controller';

// Composant
export default function AvatarField({ value, onChange, size = 256 }) {
    // const controller = useAvatarFieldController(value, onChange);

    return (
        <Grid container spacing={3}>
            <Grid item xs={12} style={{ textAlign: 'center' }}>
                <Dropzone /*onDrop={controller.handleDrop}*/ noClick noKeyboard style={{ width: size, height: size }}>
                    {({ getRootProps, getInputProps }) => (
                        <div {...getRootProps()}>
                            <AvatarEditor
                                // ref={controller.avatarEditorRef}
                                width={size}
                                height={size}
                                border={0}
                                borderRadius={size / 2}
                                // image={controller.image}
                                // scale={controller.scale}
                                // position={controller.position}
                                // onPositionChange={controller.handlePositionChange}
                            />
                            <input {...getInputProps()} />
                        </div>
                    )}
                </Dropzone>
            </Grid>
            <Grid item>
                <Typography variant="caption">x1</Typography>
            </Grid>
            <Grid item xs>
                <Slider /*value={controller.scale * 100 - 100} onChange={controller.handleScaleChange}*/ />
            </Grid>
            <Grid item>
                <Typography variant="caption">x2</Typography>
            </Grid>
        </Grid>
    );
}

AvatarField.propTypes = {
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    size: PropTypes.number
};
