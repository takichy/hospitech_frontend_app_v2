/**
 * Formulaire de demande de reset de mot de passe
 */

// React
import PropTypes from 'prop-types';

// Material UI
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';

// Controleur du formulaire
// import { useForgotPasswordFormController } from './ForgotPasswordForm.controller';

// Composant formulaire
export default function ForgotPasswordForm({ onSubmit }) {
    // const controller = useForgotPasswordFormController({ onSubmit });

    return (
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <TextField
                    required
                    fullWidth
                    label="email"
                    type="email"
                    // value={controller.email}
                    // onChange={controller.handleEmailChange}
                    // error={controller.emailError}
                    // helperText={controller.emailHelperText}
                    autoComplete="email"
                />
            </Grid>
            <Grid item xs={12}>
                <Button /*onClick={controller.validateForm} disabled={!controller.formValid}*/ variant="contained" color="primary">
                    Demander la réinitialisation de mon mot de passe
                </Button>
            </Grid>
        </Grid>
    );
}

ForgotPasswordForm.propTypes = {
    onSubmit: PropTypes.func.isRequired
};
