/**
 * Formulaire de modification du profile
 */

// React
import PropTypes from 'prop-types';

// Material-UI
import Grid from '@material-ui/core/Grid';

import TextField from '../formik/TextField';

// Formik
import { Form, Formik } from 'formik';
import { Field } from 'formik';
import { Button } from '@material-ui/core';

import * as yup from 'yup';

// Composant
export default function ProfileForm({ profile, onSubmit }) {
    // Schéma de validation du formulaire
    const formSchema = yup.object().shape({
        firstname: yup.string().required('Un prénom est obligatoire'),
        lastname: yup.string().required('Un nom est obligatoire')
    });

    return (
        <Formik initialValues={{ profile }} validationSchema={formSchema} validateOnMount onSubmit={values => onSubmit(values)}>
            {({ isSubmitting, isValid, errors, touched }) => (
                <Form>
                    <Grid item container xs={12} spacing={3}>
                        <Grid item xs={12}>
                            <Field
                                component={TextField}
                                name="firstname"
                                id="firstname"
                                autoComplete="given-name"
                                fullWidth
                                label="Prénom"
                                margin="normal"
                                required
                                helperText={errors.firstname && touched.firstname ? errors.firstname : ''}
                                data-testid="firstname"
                                error={errors.firstname && touched.firstname}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Field
                                component={TextField}
                                name="lastname"
                                id="lastname"
                                autoComplete="family-name"
                                fullWidth
                                label="Nom"
                                margin="normal"
                                error={errors.lastname && touched.lastname}
                                required
                                helperText={errors.lastname && touched.lastname ? errors.lastname : ''}
                                data-testid="lastname"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Field component={TextField} name="role" id="role" fullWidth label="Role" disabled />
                        </Grid>
                        <Grid item xs={12}>
                            <Field component={TextField} name="licence" id="licence" fullWidth label="Licence" disabled />
                        </Grid>
                        <Grid item xs={12}>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                disabled={isSubmitting || !isValid}
                                data-testid="submit"
                            >
                                SE CONNECTER
                            </Button>
                        </Grid>
                    </Grid>
                </Form>
            )}
        </Formik>
    );
}

ProfileForm.propTypes = {
    profile: PropTypes.shape({
        _id: PropTypes.string.isRequired,
        email: PropTypes.string.isRequired,
        password: PropTypes.string.isRequired,
        firstname: PropTypes.string.isRequired,
        lastname: PropTypes.string.isRequired,
        birthdate: PropTypes.string,
        num_ss: PropTypes.string,
        profilImg: PropTypes.string,
        role: PropTypes.string.isRequired,
        licence: PropTypes.string.isRequired,
        orders: PropTypes.array.isRequired
    }),
    onSubmit: PropTypes.func.isRequired
};
