/**
 * Formulaire de souscription d'un nouveau compte
 */

// React
import PropTypes from 'prop-types';

import * as yup from 'yup';

// Material UI
import Button from '@material-ui/core/Button';
import TextField from '../formik/TextField';

// Formulaire
import { Formik, Field, Form } from 'formik';

// Styles
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
    form: {
        width: '50%',
        padding: '35px',
        backgroundColor: 'white',
        marginLeft: '25%',
        marginRight: '25%'
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    },
    error: {
        backgroundColor: 'red'
    }
}));

// Composant formulaire
export default function SubscribeForm({ onSubmit }) {
    const classes = useStyles();

    // Schéma de validation du formulaire
    const formSchema = yup.object().shape({
        firstName: yup.string().required("Un Prénom pour l'utilisateur est obligatoire"),
        lastName: yup.string().required("Un nom pour l'utilisateur est obligatoire"),
        email: yup.string().email('Veuiller entrer un email valide').required('Un email est obligatoire'),
        password: yup
            .string()
            .required('Un mot de passe est obligatoire')
            .matches(
                /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
                'Doit contenir 8 caractères, une majuscule, une minuscule, un chiffre et un caractère spécial'
            ),
        confirmPassword: yup
            .string()
            .required('Un mot de passe est obligatoire')
            .matches(
                /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
                'Doit contenir 8 caractères, une majuscule, une minuscule, un chiffre et un caractère spécial'
            )
            .oneOf([yup.ref('password'), null], "la confirmation du mot de passe n'est pas identique.")
    });

    return (
        <Formik
            initialValues={{ firstName: '', lastName: '', email: '', hopitalName: '', password: '', confirmPassword: '' }}
            validationSchema={formSchema}
            validateOnMount
            onSubmit={values => onSubmit(values)}
        >
            {({ isSubmitting, isValid, errors, touched }) => (
                <Form className={classes.form}>
                    <Field
                        type="text"
                        name="firstName"
                        label="Prénom"
                        component={TextField}
                        margin="normal"
                        required
                        fullWidth
                        autoFocus
                        error={errors.firstName && touched.firstName}
                        helperText={errors.firstName && touched.firstName ? errors.firstName : ''}
                    />
                    <Field
                        type="text"
                        name="lastName"
                        label="Nom"
                        component={TextField}
                        margin="normal"
                        required
                        fullWidth
                        autoFocus
                        error={errors.lastName && touched.lastName}
                        helperText={errors.lastName && touched.lastName ? errors.lastName : ''}
                    />
                    <Field
                        type="email"
                        name="email"
                        label="Adresse email"
                        component={TextField}
                        margin="normal"
                        required
                        fullWidth
                        autoComplete="email"
                        autoFocus
                        error={errors.email && touched.email}
                        helperText={errors.email && touched.email ? errors.email : ''}
                        data-testid="email"
                    />
                    <Field
                        type="text"
                        name="hopitalName"
                        label="Nom d'hôpital"
                        component={TextField}
                        margin="normal"
                        required
                        fullWidth
                        autoFocus
                        error={errors.hopitalName && touched.hopitalName}
                        helperText={errors.hopitalName && touched.hopitalName ? errors.hopitalName : ''}
                    />
                    <Field
                        type="password"
                        name="password"
                        label="Mot de passe"
                        component={TextField}
                        margin="normal"
                        required
                        fullWidth
                        error={errors.password && touched.password}
                        helperText={errors.password && touched.password ? errors.password : ''}
                        data-testid="password"
                    />
                    <Field
                        type="password"
                        name="confirmPassword"
                        label="Confirmation mot de passe"
                        component={TextField}
                        margin="normal"
                        required
                        fullWidth
                        error={errors.confirmPassword && touched.confirmPassword}
                        helperText={errors.confirmPassword && touched.confirmPassword ? errors.confirmPassword : ''}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        disabled={isSubmitting || !isValid}
                        className={classes.submit}
                        data-testid="submit"
                    >
                        Valider
                    </Button>
                </Form>
            )}
        </Formik>
    );
}

SubscribeForm.propTypes = {
    onSubmit: PropTypes.func.isRequired
};
