/**
 * Table des stocks
 */

import PropTypes from 'prop-types';

import HtechTable from '../commons/HtechTable';

function StockTable({ stocks }) {
    const data = stocks;

    // Définition des colonnes
    const columns = [
        {
            name: 'name',
            label: 'StockTableName'
        },
        {
            name: 'refNumber',
            label: 'StockTableRefNumber'
        },
        {
            name: 'inStock',
            label: 'StockTableInStock'
        },
        {
            name: 'inCommand',
            label: 'StockTableInCommand'
        }
    ];

    // Options
    const options = {
        selectableRows: 'none',
        download: false,
        elevation: 1,
        filter: false,
        print: false,
        viewColumns: false
    };

    // Rendu
    return <HtechTable data={data} columns={columns} options={options} />;
}

StockTable.propTypes = {
    stocks: PropTypes.array.isRequired
};

export default StockTable;
