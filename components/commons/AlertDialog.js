/**
 * Composant d'affichage d'un message d'alerte sous forme de dialogue avec
 * validation de l'utilisateur.
 */

// React
import React from 'react';
import PropTypes from 'prop-types';

// Material-UI
import DialogContentText from '@material-ui/core/DialogContentText';

// Composants applicatifs
import { DialogFrame } from './DialogFrame';

// Autres
const R = require('ramda');

// Constantes pour la spécification des boutons
const AlertDialogButtonType = Object.freeze({
    OK: 0,
    OK_CANCEL: 1,
    CANCEL: 2
});

export { AlertDialogButtonType };

// Composant de dialogue
export default function AlertDialog({
    open,
    title,
    message,
    onClose,
    buttonType = AlertDialogButtonType.OK,
    buttonOptions,
    ...props
}) {
    const _buttonOptions = R.mergeDeepRight(
        {
            OK: {
                label: 'Confirmer',
                color: 'primary',
                variant: 'text'
            },
            CANCEL: {
                label: 'Abandonner',
                color: 'primary',
                variant: 'text'
            }
        },
        buttonOptions || {}
    );

    const buttons = [];
    if (buttonType === AlertDialogButtonType.OK_CANCEL || buttonType === AlertDialogButtonType.CANCEL) {
        buttons.push({
            ..._buttonOptions.CANCEL,
            default: false,
            onClick: () => onClose(false)
        });
    }

    if (buttonType === AlertDialogButtonType.OK_CANCEL || buttonType === AlertDialogButtonType.OK) {
        buttons.push({
            ..._buttonOptions.OK,
            default: true,
            onClick: () => onClose(true)
        });
    }

    return (
        <DialogFrame open={open} title={title} buttons={buttons} onClose={onClose} {...props}>
            <DialogContentText id="alert-dialog-description">{message}</DialogContentText>
        </DialogFrame>
    );
}

AlertDialog.propTypes = {
    open: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
    onClose: PropTypes.func.isRequired,
    buttonType: PropTypes.oneOf([AlertDialogButtonType.OK, AlertDialogButtonType.OK_CANCEL, AlertDialogButtonType.CANCEL]),
    buttonOptions: PropTypes.shape({
        OK: PropTypes.shape({
            label: PropTypes.string,
            color: PropTypes.string,
            variant: PropTypes.oneOf(['text', 'outlined', 'contained'])
        }),
        CANCEL: PropTypes.shape({
            label: PropTypes.string,
            color: PropTypes.string,
            variant: PropTypes.oneOf(['text', 'outlined', 'contained'])
        })
    })
};
