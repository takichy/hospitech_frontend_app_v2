/**
 * Fenêtre de dialogue par défaut
 */

// React
import React from 'react';
import PropTypes from 'prop-types';

// Material UI
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

// Styles
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles(theme => ({
    dialogTitle: {
        //backgroundColor: theme.palette.primary.main,
        backgroundImage: 'linear-gradient(171deg,#200056 17%,#8f42ec 100%)',
        color: theme.palette.primary.contrastText,
        textAlign: 'center'
    },
    dialogContent: {
        padding: `${theme.spacing(6)}px ${theme.spacing(6)}px ${theme.spacing(6)}px ${theme.spacing(6)}px`
    },
    dialogActions: {
        padding: `${theme.spacing(3)}px ${theme.spacing(6)}px ${theme.spacing(3)}px ${theme.spacing(6)}px`
    }
}));

// Dialog
function DialogFrame({ open, onClose, title, buttons, children, ...props }) {
    const classes = useStyles();

    const localOnClose = (event, reason) => {
        if (reason !== 'backdropClick') {
            onClose(event, reason);
        }
    };

    return (
        <Dialog {...props} open={open} onClose={localOnClose} disableEscapeKeyDown>
            <DialogTitle className={classes.dialogTitle}>{title}</DialogTitle>
            <DialogContent dividers className={classes.dialogContent}>
                {children}
            </DialogContent>
            {buttons && buttons.length > 0 && (
                <DialogActions className={classes.dialogActions}>
                    {buttons.map(button => (
                        <Button
                            key={button.label}
                            onClick={button.onClick}
                            variant={button.variant || (button.default ? 'contained' : 'text')}
                            type="submit"
                            color={button.color || 'primary'}
                            disabled={!!button.disabled}
                        >
                            {button.label}
                        </Button>
                    ))}
                </DialogActions>
            )}
        </Dialog>
    );
}

DialogFrame.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    buttons: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string.isRequired,
            default: PropTypes.bool.isRequired,
            onClick: PropTypes.func.isRequired,
            disabled: PropTypes.bool,
            variant: PropTypes.string,
            color: PropTypes.string
        })
    ),
    children: PropTypes.any.isRequired
};

export { DialogFrame };
