/**
 * Template pour les pages non sécurisées.
 * @module components/commons/DisconnectedLayout
 */

// Nom de l'application
const { title, version } = require('../../package');

// React
import { Fragment } from 'react';
import PropTypes from 'prop-types';

// Material
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

// Style
import { makeStyles } from '@material-ui/styles';
const useStyles = makeStyles(theme => ({
    root: {
        height: '100vh',
        justifyContent: 'center',
        alignItems: 'center'
    },
    brand: {
        backgroundColor: theme.palette.primary.main
    },
    logo: {
        maxWidth: '31%',
        margin: theme.spacing(5)
    },
    information: {
        marginTop: theme.spacing(2),
        maxWidth: '61%',
        color: theme.palette.primary.contrastText
    },
    header: {
        flex: '1 1 auto',
        marginBottom: theme.spacing(3),
        justifyContent: 'flex-end'
    },
    footer: {
        flex: '1 1 auto',
        marginBottom: theme.spacing(3),
        justifyContent: 'center',
        alignItems: 'flex-end'
    }
}));

// Header du paneau droit
function Header(props) {
    const classes = useStyles();

    return (
        <Fragment>
            <Grid container direction="column" className={classes.header} />
            <img src="/static/img/logo.png" className={classes.logo} />
            <Typography component="h1" variant="h5">
                {props.title}
            </Typography>
        </Fragment>
    );
}

Header.propTypes = {
    title: PropTypes.string.isRequired
};

// Footer du paneau droit
function Footer() {
    const classes = useStyles();

    return (
        <Grid container className={classes.footer}>
            <Typography variant="body2" color="textSecondary" align="center">
                {`${title} v${version}`} -
            </Typography>
            <Typography variant="body2" color="textSecondary" align="center">
                &nbsp;Développé par l&apos;équipe
                <Link target="_blank" href="https://www.hospitech.fr">
                    &nbsp;Hospitech&nbsp;
                </Link>
                team.
            </Typography>
        </Grid>
    );
}

// Composant
export default function DisconnectedLayout(props) {
    const classes = useStyles();

    return (
        <Grid direction="column" container className={classes.root}>
            {/* Côté droit, le formulaire de login par exemple */}
            <Header title={props.title} />
            {props.children}
            <Footer />
        </Grid>
    );
}

DisconnectedLayout.propTypes = {
    title: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired
};
