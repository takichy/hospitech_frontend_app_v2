/**
 * Menu popup générique
 * @module components/commons/HtechMenu
 */

// React
import PropTypes from 'prop-types';

// Material-UI
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Icon from '@material-ui/core/Icon';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

/**
 * Composant UserMenu
 */
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
    icon: {
        minWidth: '30px'
    }
}));
export default function HtechMenu({ anchor, onClose, items }) {
    const classes = useStyles();

    const handleClick = item => event => {
        event.stopPropagation();
        item.onSelect(event);
        onClose(event);
    };

    return (
        <Menu
            anchorEl={anchor}
            keepMounted
            onClick={e => e.stopPropagation()}
            open={Boolean(anchor)}
            onClose={onClose}
            data-testid="menu"
        >
            {items.map((item, i) => (
                <MenuItem key={i} onClick={handleClick(item)} disabled={item.disabled}>
                    <ListItemIcon className={classes.icon}>
                        <Icon>{item.iconName && item.iconName}</Icon>
                    </ListItemIcon>
                    <ListItemText primary={item.name} />
                </MenuItem>
            ))}
        </Menu>
    );
}

HtechMenu.propTypes = {
    anchor: PropTypes.object,
    onClose: PropTypes.func.isRequired,
    items: PropTypes.array.isRequired
};
