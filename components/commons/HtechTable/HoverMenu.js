/**
 * Menu flottant au dessus d'une ligne de tableau
 */

// Proptypes
import PropTypes from 'prop-types';

// Material UI
import Box from '@material-ui/core/Box';
import Tooltip from '@material-ui/core/Tooltip';

// Composant
export default function HoverMenu({ items }) {
    return (
        <Box onClick={e => e.stopPropagation()} height={24}>
            {items.map((item, index) => {
                const Icon = item.iconComponent;
                return (
                    <Box key={index} display="inline-block" height={24} marginLeft={3}>
                        {item.tooltip ? (
                            <Tooltip title={item.tooltip} arrow>
                                {/* encapsulation dans un span pour être certain d'être compatible avec les tooltips */}
                                <span>
                                    <Icon
                                        onClick={item.action ?? (() => {})}
                                        color="action"
                                        {...(item.props ?? {})}
                                        fontSize="default"
                                    />
                                </span>
                            </Tooltip>
                        ) : (
                            <Icon onClick={item.action ?? (() => {})} color="action" {...(item.props ?? {})} fontSize="default" />
                        )}
                    </Box>
                );
            })}
        </Box>
    );
}

HoverMenu.propTypes = {
    items: PropTypes.array.isRequired
};
