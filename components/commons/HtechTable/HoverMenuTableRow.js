/**
 * Ligne de tableau customisée pour afficher un hover menu
 */

// React PropTypes
import PropTypes from 'prop-types';

// Material UI
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

// Composants applicatifs
import MenuTable from '../MenuTable';
import HoverMenu from './HoverMenu';

// Styles
import { makeStyles } from '@material-ui/styles';
import clsx from 'clsx';

// Hypothèse : les items de menu sont des icônes de taille standard 24x24 et les autres éléments de la table sont
// du texte body1
const useStyles = makeStyles(theme => ({
    hoverMenuCell: {
        position: 'absolute',
        right: 62, // en dur, marge pour le menu contextuel
        display: 'inline-block',
        background: 'linear-gradient(90deg, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 15%)',
        paddingLeft: theme.spacing(8),
        borderRight: '1px solid #E0E0E0'
    },

    hoverMenuCellSizeStandard: {
        paddingTop: 14, // Ajustement car icone hauteur 24 et texte hauteur 20 => padding de 16 remplacé par 14
        paddingBottom: 14
    },

    hoverMenuCellSizeLarge: {
        paddingTop: 18, // Ajustement car icone hauteur 24 et ligne hauteur 29 => padding de 16 remplacé par 18
        paddingBottom: 19
    }
}));

// Composant
export default function HoverMenuTableRow({
    options,
    columns,
    data,
    dataIndex,
    rowIndex,
    rowMenu,
    hoverMenu,
    onMouseEnter,
    onMouseLeave,
    showHoverMenu
}) {
    const classes = useStyles();

    // Menu Contextuel
    const menuRender = value => <MenuTable data={value} />;

    // Contenu de la cellule
    const tableCellContent = (column, value) => {
        if (column.options?.customBodyRender) {
            return column.options.customBodyRender(value);
        } else if (column.options?.customBodyRenderLite) {
            return column.options.customBodyRenderLite(dataIndex, rowIndex);
        }

        return value;
    };

    return (
        <TableRow
            hover={options.rowHover}
            onClick={event => options.onRowClick(data, { dataIndex, rowIndex }, event)}
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
            style={{ ...(options.onRowClick ? { cursor: 'pointer' } : {}) }}
        >
            {columns.map((column, index) => {
                const value = data[index];

                return (
                    (column.options?.display ?? true) &&
                    column.options?.display !== 'excluded' && (
                        <TableCell key={column.name} {...(column.options?.setCellProps ? column.options.setCellProps() : {})}>
                            {tableCellContent(column, value)}
                        </TableCell>
                    )
                );
            })}
            <TableCell key="rowMenu">{menuRender(rowMenu)}</TableCell>
            {showHoverMenu && hoverMenu && (
                <TableCell
                    key="hoverMenu"
                    className={clsx(
                        classes.hoverMenuCell,
                        options.hoverMenuSize === 'large' ? classes.hoverMenuCellSizeLarge : classes.hoverMenuCellSizeStandard
                    )}
                >
                    <HoverMenu items={hoverMenu} />
                </TableCell>
            )}
        </TableRow>
    );
}

HoverMenuTableRow.propTypes = {
    options: PropTypes.object.isRequired,
    columns: PropTypes.array.isRequired,
    data: PropTypes.array.isRequired,
    dataIndex: PropTypes.number.isRequired,
    rowIndex: PropTypes.number.isRequired,
    rowMenu: PropTypes.array,
    hoverMenu: PropTypes.array,
    onMouseEnter: PropTypes.func.isRequired,
    onMouseLeave: PropTypes.func.isRequired,
    showHoverMenu: PropTypes.bool.isRequired
};
