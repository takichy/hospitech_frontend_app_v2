/**
 */

// React PropTypes
import { useState } from 'react';
import PropTypes from 'prop-types';

// MuiDatatables
import MUIDataTable from 'mui-datatables';

// Composants applicatifs
import MenuTable from '../MenuTable';
import HoverMenuTableRow from './HoverMenuTableRow';

// Composant
export default function HtechTable({ data, columns, options = {}, ...props }) {
    if (!columns && !Array.isArray(columns)) {
        throw new Error('columns property cannot be null or undefined and must be an array');
    }

    const textLabels = {
        body: {
            noMatch: 'noMatchingRecordsFound',
            toolTip: 'Sort'
        },
        pagination: {
            next: 'nextPage',
            previous: 'previousPage',
            rowsPerPage: 'rowsPerPage',
            displayRows: 'of'
        },
        toolbar: {
            search: 'Search',
            downloadCsv: 'downloadCSV',
            print: 'Print',
            viewColumns: 'viewColumns',
            filterTable: 'filterTable'
        },
        filter: {
            all: 'All',
            title: 'FILTERS',
            reset: 'RESET'
        },
        viewColumns: {
            title: 'showColumns',
            titleAria: 'Show/hideTableColumns'
        },
        selectedRows: {
            text: 'rowsDeleted',
            delete: 'Delete',
            deleteAria: 'deleteSelectedRows'
        }
    };

    // Support du hoverMenu au travers d'une ligne customisée
    // => d'où le non support des lignes sélectionnables
    const [hoverRowIndex, setHoverRowIndex] = useState(null); // sur quelle ligne afficher le hoverMenu

    const rowRender = (rowData, dataIndex, rowIndex) => {
        const handleMouseEnter = () => setHoverRowIndex(rowIndex);
        const handleMouseLeave = () => setHoverRowIndex(null);

        return (
            <HoverMenuTableRow
                key={dataIndex}
                data={rowData}
                columns={columns}
                options={{ rowHover: true, ...options }}
                dataIndex={dataIndex}
                rowIndex={rowIndex}
                rowMenu={data[dataIndex].rowMenu}
                hoverMenu={data[dataIndex].hoverMenu}
                onMouseEnter={handleMouseEnter}
                onMouseLeave={handleMouseLeave}
                showHoverMenu={rowIndex === hoverRowIndex}
            />
        );
    };

    // Déclarer le custom body render
    // Non compatible avec des lignes sélectionnables
    const fullOptions = {
        rowHover: true,
        hoverMenuSize: 'standard', // 'large' si la ligne contient des icônes
        setRowProps: () => ({ style: { cursor: 'pointer' } }),
        setFilterChipProps: () => ({
            color: 'primary'
        }),
        textLabels,
        ...options,
        ...(options.hoverMenu
            ? {
                  selectableRows: 'none',
                  customRowRender: rowRender
              }
            : {})
    };

    // Ajout du menu contextuel
    const menuRender = value => <MenuTable data={value} />;

    const fullColumns = [
        ...columns,
        ...(options.rowMenuHidden
            ? []
            : [
                  {
                      label: ' ',
                      name: 'rowMenu',
                      options: {
                          customBodyRender: menuRender,
                          download: false,
                          draggable: false,
                          filter: false,
                          print: false,
                          searchable: false,
                          sort: false,
                          viewColumns: false,
                          setCellHeaderProps: () => ({ style: { width: 32 } })
                      }
                  }
              ])
    ];

    return <MUIDataTable {...props} data={data} columns={fullColumns} options={fullOptions} />;
}

HtechTable.propTypes = {
    data: PropTypes.array.isRequired,
    columns: PropTypes.array.isRequired,
    options: PropTypes.object
};
