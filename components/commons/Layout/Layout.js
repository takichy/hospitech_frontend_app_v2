// React
import PropTypes from 'prop-types';
import clsx from 'clsx';

// Material UI
import LinearProgress from '@material-ui/core/LinearProgress';

// Composants applicatifs
import NavigationMenu from './NavigationMenu';
import NavigationTitle from './NavigationTitle';
import StatusMessage from './StatusMessage';
import TopAppBar from './TopAppBar';

// Styles
import { useStyles } from './Layout.styles';

// React, Next
import { useState } from 'react';

// Layout principal
export default function Layout({ progress, user, title = '', back = false, contentClassName, children }) {
    const classes = useStyles();

    const childrenClassName = contentClassName || classes.default;

    const [navigationMenuOpen, setNavigationMenuOpen] = useState(false);

    // Forcer l'ouverture
    function openNavigationMenu() {
        setNavigationMenuOpen(true);
    }

    // Forcer la fermeture
    function closeNavigationMenu() {
        setNavigationMenuOpen(false);
    }

    return (
        <div className={classes.root}>
            <TopAppBar open={navigationMenuOpen} user={user} openDrawer={openNavigationMenu} />
            <NavigationMenu open={navigationMenuOpen} user={user} closeDrawer={closeNavigationMenu} />
            <main className={clsx(classes.content, { [classes.contentShift]: navigationMenuOpen })}>
                <div className={classes.drawerHeader} />
                <NavigationTitle title={title} back={back} />
                {progress.inProgress && <LinearProgress color="secondary" />}
                <StatusMessage progress={progress} />
                <div className={clsx(classes.main, childrenClassName)}>{children}</div>
            </main>
        </div>
    );
}

Layout.propTypes = {
    progress: PropTypes.shape({
        inProgress: PropTypes.bool.isRequired,
        showMessage: PropTypes.bool.isRequired,
        status: PropTypes.oneOf(['none', 'success', 'info', 'warning', 'error']).isRequired,
        message: PropTypes.string.isRequired
    }).isRequired,
    title: PropTypes.string,
    back: PropTypes.bool,
    user: PropTypes.object.isRequired,
    children: PropTypes.node.isRequired,
    contentClassName: PropTypes.string
};

Layout.defaultProps = {
    progress: {
        inProgress: false,
        showMessage: false,
        status: 'info',
        message: ''
    },
    title: '',
    back: false,
    contentClassName: '',
    user: { lastName: '', firstName: '' }
};
