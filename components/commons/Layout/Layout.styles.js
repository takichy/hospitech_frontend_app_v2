/**
 * Styles du composant Layout
 */

import { makeStyles } from '@material-ui/styles';

const drawerWidth = 300;
const useStyles = makeStyles(theme => ({
    default: {
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        alignContent: 'flex-start',
        padding: theme.spacing(3)
    },

    // Style de la zone de contenu de la page
    // Comme pour la barre d'application, s'anime quand le menu ouvre ou se ferme
    // On applique également une margine intérieure (padding) de 3x8
    content: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        }),
        marginLeft: 0
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen
        }),
        marginLeft: drawerWidth
    },

    // Entete du menu de navigation
    // Permet de se caler sur la hauteur de la barre d'application
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end'
    },

    // Une zone de marge pour rétablir l'équilibre.
    contentMarging: {
        marginBottom: theme.spacing(3)
    },

    main: {
        display: 'flex',
        flexGrow: 1
    }
}));

export { useStyles };
