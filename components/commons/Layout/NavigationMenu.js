// Information sur l'application
const appTitle = require('../../../package').title;
const appVersion = require('../../../package').version;

// React
import PropTypes from 'prop-types';

// Next
// Utilisé pour les liens vers les autres pages.
import Link from 'next/link';
import { useRouter } from 'next/router';

// Material UI
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';

import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

// Styles
import { useStyles } from './NavigationMenu.styles';

// Menu de navigation (drawer)
import { menuItems } from '../../../config/menu-items';

import clsx from 'clsx';

export default function NavigationMenu({ user, open, closeDrawer }) {
    const classes = useStyles();
    const router = useRouter();

    // Item de menu
    const MenuItem = ({ icon, text, path, id }) => (
        <Link href={path} prefetch={false}>
            <ListItem button selected={router.pathname === path} data-testid={`menuitem-${id}`}>
                <ListItemIcon>
                    <Icon>{icon}</Icon>
                </ListItemIcon>
                <ListItemText primary={text} />
            </ListItem>
        </Link>
    );

    MenuItem.propTypes = {
        icon: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired,
        path: PropTypes.string.isRequired,
        id: PropTypes.string.isRequired
    };

    // En-tête du menu
    const menuHeader = (
        <div className={classes.drawerHeader}>
            <div className={classes.appTitle}>
                <Typography variant="h6">{appTitle}</Typography>
                <Typography variant="body2">v{appVersion}</Typography>
            </div>
            <IconButton onClick={closeDrawer}>
                <ChevronLeftIcon />
            </IconButton>
        </div>
    );

    // Menu de navigation
    return (
        <Drawer
            variant="persistent"
            open={open}
            anchor="left"
            className={clsx(classes.drawer, {
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open
            })}
            classes={{
                paper: clsx(classes.drawerPaper, {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                    [classes.drawerPaperHover]: !open
                })
            }}

            // className={classes.drawer} classes={{ paper: classes.drawerPaper }}
        >
            {menuHeader}
            <Divider />
            <List>
                {menuItems(user.scope).map(item => (
                    <MenuItem key={item.itemKey} id={item.itemKey} icon={item.icon} text={item.name} path={item.path} />
                ))}
            </List>
        </Drawer>
    );
}

NavigationMenu.propTypes = {
    user: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    closeDrawer: PropTypes.func.isRequired
};
