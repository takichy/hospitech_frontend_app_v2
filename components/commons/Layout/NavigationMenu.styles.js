/**
 * Styles du composant NavigationMenu
 */

import { makeStyles } from '@material-ui/styles';

const drawerWidth = 300;
const useStyles = makeStyles(theme => ({
    // Titre de l'application dans le header du menu de navigation
    // Le bloc inclut également la version
    appTitle: {
        flexGrow: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        marginLeft: theme.spacing(2),
        color: theme.palette.text.secondary
    },

    // Entete du menu de navigation
    // Permet de se caler sur la hauteur de la barre d'application
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
        // backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText
    },

    // Menu de navigation : largeur fixe
    drawer: {
        // width: drawerWidth,
        // flexShrink: 0
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
        zIndex: theme.zIndex.drawer
    },

    drawerPaper: {
        // width: drawerWidth
        backgroundColor: theme.palette.background.default,
        border: [[0], '!important']
    },
    drawerPaperHover: {
        '&:hover': {
            width: drawerWidth,
            transition: theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen
            }),
            boxShadow: theme.shadows[3]
        }
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9)
        }
    }
}));

export { useStyles };
