/**
 * Titre en tête de chaque page
 */

// React
import PropTypes from 'prop-types';

// Next
// Utilisé pour les liens vers les autres pages.
import { useRouter } from 'next/router';

// Material UI
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import ArrowBackIos from '@material-ui/icons/ArrowBackIos';

// Styles
import { makeStyles } from '@material-ui/styles';
const useStyles = makeStyles(theme => ({
    title: {
        padding: theme.spacing(3)
    },

    titleText: {
        alignSelf: 'center'
    }
}));

// Titre de la page (avec navigation)
export default function NavigationTitle({ back, title }) {
    const classes = useStyles();
    const router = useRouter();

    return (
        <Paper square elevation={0} variant="outlined" className={classes.title}>
            <Grid container alignItems="center">
                {back && (
                    <Grid item>
                        <IconButton
                            onClick={() => {
                                router.back();
                            }}
                        >
                            <ArrowBackIos />
                        </IconButton>
                    </Grid>
                )}
                <Grid item className={classes.titleText}>
                    <Typography variant="h5">{title}</Typography>
                </Grid>
            </Grid>
        </Paper>
    );
}

NavigationTitle.propTypes = {
    back: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired
};
