/**
 * Message d'état d'une opération
 */

// React
import PropTypes from 'prop-types';

// Material UI
import Alert from '@material-ui/lab/Alert';
import Box from '@material-ui/core/Box';
import Slide from '@material-ui/core/Slide';

// Composant
export default function StatusMessage({ progress }) {
    return (
        <Slide in={progress.showMessage} direction="left" mountOnEnter unmountOnExit>
            <Box position="absolute" top={80} right={16} zIndex={10}>
                <Alert severity={progress.status} variant="filled" data-testid="status">
                    {progress.message}
                </Alert>
            </Box>
        </Slide>
    );
}

StatusMessage.propTypes = {
    progress: PropTypes.shape({
        showMessage: PropTypes.bool.isRequired,
        status: PropTypes.oneOf(['none', 'success', 'info', 'warning', 'error']).isRequired,
        message: PropTypes.string.isRequired
    }).isRequired
};
