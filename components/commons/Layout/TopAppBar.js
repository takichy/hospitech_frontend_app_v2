/**
 * Barre supérieur de l'application.
 * En mode barre de navigation fermée :
 *  - bouton menu pour ouvrir la barre de navigation
 *  - logo
 *  - Titre
 * En mode ouvert
 *  - Titre
 *
 * Puis à droite
 * - Menu utilisateur
 * - nom de l'utilisateur
 * - sélecteur de langue
 * - indicateur de sauvegarde automatique
 */

// Information sur l'application
const appTitle = require('../../../package').title;

// React
import PropTypes from 'prop-types';
import clsx from 'clsx';

// Material UI
import AppBar from '@material-ui/core/AppBar';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import MenuIcon from '@material-ui/icons/Menu';

import UserMenu from './UserMenu';

// Styles
import { useStyles } from './TopAppBar.styles';

// Barre d'application
export default function TopAppBar({ user, open, openDrawer }) {
    const classes = useStyles();

    return (
        <AppBar position="fixed" className={clsx(classes.appBar, { [classes.appBarShift]: open })}>
            <Toolbar>
                <IconButton
                    color="inherit"
                    className={clsx(classes.menuButton, open && classes.hide)}
                    onClick={openDrawer}
                    edge="start"
                >
                    <MenuIcon />
                </IconButton>

                {!open && <Avatar variant="square" alt="Hospitech App" src="/static/img/logo.png" />}

                <Typography variant="h6" color="inherit" className={classes.grow}>
                    {!open && appTitle}
                </Typography>

                <Typography color="inherit" data-testid="username">{`${user.firstName} ${user.lastName}`}</Typography>
                <UserMenu
                    initials={`${user.firstName ? user.firstName.charAt(0) : ''}${user.lastName ? user.lastName.charAt(0) : ''}`}
                />
            </Toolbar>
        </AppBar>
    );
}

TopAppBar.propTypes = {
    user: PropTypes.shape({ firstName: PropTypes.string.isRequired, lastName: PropTypes.string.isRequired }),
    openDrawer: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired
};
