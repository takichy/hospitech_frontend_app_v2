/**
 * Styles du composant TopAppBar
 */
import { makeStyles } from '@material-ui/styles';

const drawerWidth = 300;
const useStyles = makeStyles(theme => ({
    // Style de la barre d'application
    // Usage d'animation lors de la rétractation ou l'ouverture du menu
    appBar: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen
        })
    },

    // Une marge pour le bouton de déclenchement du menu
    // Hide pour cacher le bouton quand le menu est ouvert
    menuButton: {
        marginRight: theme.spacing(3)
    },

    hide: {
        display: 'none'
    },

    // Utilisé pour indiquer le titre de l'application dans la barre doit s'étendre
    // Permet de "pousser" le nom de l'utilisateur et le menu vers l'extrême droite
    grow: {
        flexGrow: 1,
        marginLeft: theme.spacing(3)
    }
}));

export { useStyles };
