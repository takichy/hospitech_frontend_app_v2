/**
 * Menu utilisateur à droite de la barre de titre
 */

// React
import { Fragment, useState } from 'react';
import PropTypes from 'prop-types';

// Material UI
import Avatar from '@material-ui/core/Avatar';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

// Styles
import { makeStyles } from '@material-ui/styles';
const useStyles = makeStyles(theme => ({
    avatar: {
        backgroundColor: theme.palette.primary.contrastText,
        color: theme.palette.primary.main,
        marginLeft: theme.spacing(2),
        cursor: 'pointer'
    }
}));

// Menu utilisateur à droite de la barre d'application
export default function UserMenu({ initials }) {
    const [anchor, setAnchor] = useState(null);
    const classes = useStyles();

    // Gestion de l'attachement du menu
    function handleClick(event) {
        setAnchor(event.currentTarget);
    }

    function handleClose() {
        setAnchor(null);
    }

    // Dialogue de changement de mot de passe
    function handleChangePasswordSelect() {
        setAnchor(null);
    }

    return (
        <Fragment>
            <Avatar onClick={handleClick} className={classes.avatar}></Avatar>
            <Menu
                id="menu-appbar"
                anchorEl={anchor}
                getContentAnchorEl={null}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right'
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right'
                }}
                open={Boolean(anchor)}
                onClose={handleClose}
                disableAutoFocusItem
            >
                <MenuItem component="a" href="/profile">
                    Profil
                </MenuItem>
                <MenuItem onClick={handleChangePasswordSelect} divider>
                    Changer le mot de passe
                </MenuItem>
                <MenuItem component="a" href="/logon">
                    Se déconnecter
                </MenuItem>
            </Menu>
            {/* <ChangePasswordDialog open={false} onConfirm={handleChangePasswordDialogConfirm} onCancel={false} /> */}
        </Fragment>
    );
}

UserMenu.propTypes = {
    initials: PropTypes.string.isRequired
};
