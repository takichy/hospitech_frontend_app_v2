import React from 'react';
import PropTypes from 'prop-types';

import InputAdornment from '@material-ui/core/InputAdornment';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import IconButton from '@material-ui/core/IconButton';

import HtechMenu from './HtechMenu';

export default function MenuTable({ data }) {
    //sub menu anchor
    const [anchor, setAnchor] = React.useState(null);

    //subMenu funtions
    const handleClick = event => {
        event.stopPropagation();
        setAnchor(event.currentTarget);
    };

    const handleClose = event => {
        event.stopPropagation();
        setAnchor(null);
    };

    return (
        <InputAdornment position="end">
            <IconButton onClick={handleClick} disabled={!(data !== null && Array.isArray(data) && data.length)} size="small">
                <MoreVertIcon />
            </IconButton>
            <HtechMenu anchor={anchor} onClose={handleClose} items={Array.isArray(data) ? data : []} />
        </InputAdornment>
    );
}

MenuTable.propTypes = {
    data: PropTypes.array
};
