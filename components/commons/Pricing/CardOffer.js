// React
import React from 'react';
import PropTypes from 'prop-types';

// material ui
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
        height: '940px'
    },
    title: {
        color: '#3DC8F4',
        fontStyle: 'normal',
        fontWeight: 500,
        fontSize: '30px',
        lineHeight: '42px',
        textAlign: 'center'
    },
    price: {
        color: '#252736',
        fontStyle: 'normal',
        fontWeight: 300,
        fontSize: '55px',
        lineHeight: '1.2em',
        textAlign: 'center'
    },
    period: {
        fontSize: '18px'
    },
    buttonPub: {
        color: '#fff !important',
        cursor: 'pointer',
        borderRadius: '3px !important',
        paddingRight: '20px !important',
        paddingLeft: '20px !important',
        paddingTop: ' 11px !important',
        paddingBottom: '11px !important',
        margin: '12% !important',
        border: '0px !important',
        fontFamily: 'Rubik',
        fontStyle: 'normal',
        fontWeight: '500 !important',
        fontSize: '18px',
        width: '100%',
        background: 'linear-gradient(270deg,#3DC8F4 0%,#3DD3F4 100.62%) !important',
        padding: '0.3em 1em',
        lineHeight: '1.7em !important',
        backgroundColor: 'transparent',
        backgroundSize: 'cover',
        backgroundPosition: '50%',
        backgroundRepeat: 'no-repeat',
        textDecoration: 'none',
        textAlign: 'center',
        justifyContent: 'center'
    },
    offers: {
        marginLeft: '10%',
        marginRight: '10%',
        color: '#252736',
        fontStyle: 'normal',
        fontWeight: 300,
        fontSize: '16px',
        lineHeight: '1.4em',
        textAlign: 'left',
        textJustify: 'center',
        marginBottom: '40px'
    },
    includes: {
        padding: '1px'
    },
    included: {
        color: '#252736',
        fontStyle: 'normal',
        fontWeight: 600,
        fontSize: '16px',
        lineHeight: ' 1.4em',
        textAlign: 'left',
        marginTop: '65px',
        marginBottom: '33px'
    }
});

export default function CardOffer({ type, price, period, listOffers, SubscriptOnPack }) {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                    <p className={classes.title}>{type}</p>

                    <p className={classes.price}>
                        {price}€<span className={classes.period}>/{period}</span>
                    </p>

                    <a className={classes.buttonPub} onClick={() => SubscriptOnPack(type)}>
                        Choisir un abonnement
                    </a>

                    <p className={classes.included}>Inclut :</p>

                    <div>
                        <div className={classes.includes}>
                            {listOffers.map(offer => {
                                return (
                                    <p className={classes.offers} key={offer.id}>
                                        🗸 {offer.name}
                                    </p>
                                );
                            })}
                        </div>
                    </div>
                </Typography>
            </CardContent>
        </Card>
    );
}

CardOffer.propTypes = {
    type: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    period: PropTypes.string.isRequired,
    listOffers: PropTypes.array.isRequired,
    SubscriptOnPack: PropTypes.func.isRequired
};
