/*
 * TextField.js
 * Wrapper d'un objet TextField pour être utilisé dans des formulaires Formik
 */

import PropTypes from 'prop-types';
import MuiTextField from '@material-ui/core/TextField';

export default function TextField({
    field, // name, value, onChange, onBlur
    ...props
}) {
    return <MuiTextField {...field} {...props} />;
}

TextField.propTypes = {
    field: PropTypes.object.isRequired
};
