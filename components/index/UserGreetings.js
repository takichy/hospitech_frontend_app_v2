/*
 * UserGreetings.js
 *
 * Accueil de l'utilisateur sur la page principale.
 * (c) HOSPITECH
 */

// React
import PropTypes from 'prop-types';

// Composant Material UI
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

export default function UserGreetings({ userName }) {
    return (
        <Grid item xs={12}>
            <Typography paragraph data-testid="greetings">
                Bienvenue {userName} !
            </Typography>
        </Grid>
    );
}

UserGreetings.propTypes = {
    userName: PropTypes.string.isRequired
};
