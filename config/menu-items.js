/**
 * Définitions des items de la barre de navigation latérale
 * @module config/menu-items
 *
 * L'identifiant des icônes fait référence à la fonte Material Icon : https://material-ui.com/components/material-icons/
 * La documentation de l'Objet Icon est https://material-ui.com/components/icons/#icon-font-icons
 */

/**
 * La liste des items de menu est fonction des habilitations de l'utilisateur
 * @param {string} scope les habilitations de l'utilisateur
 */
export function menuItems() {
    // const theScope = scope || '';

    return [
        {
            itemKey: 'home',
            name: 'Acceuil',
            icon: 'home',
            path: '/'
        },
        {
            itemKey: 'licence',
            name: 'Licence',
            icon: 'book',
            path: '/licence'
        },
        {
            itemKey: 'administration',
            name: 'Administration',
            icon: 'book',
            path: '/administration'
        },
        {
            itemKey: 'GestionStocks',
            name: 'Stocks',
            icon: 'book',
            path: '/stocks'
        }
        // ...(theScope?.includes('user.read')
        //     ? [
        //           {
        //               itemKey: 'users',
        //               name: 'users',
        //               icon: 'group',
        //               path: '/users'
        //           }
        //       ]
        //     : [])
    ];
}
