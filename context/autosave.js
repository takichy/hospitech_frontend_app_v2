/**
 * Contexte de l'indicateur d'autosave
 */

// React
import { useState, useCallback } from 'react';

// Enums
const AutosaveState = {
    OFF: 'off',
    DIRTY: 'dirty',
    SAVING: 'saving',
    SAVED: 'saved',
    ERROR: 'error'
};

export { AutosaveState };

// Contexte
export function createAutosaveContext(globalContext) {
    const [autosave, setAutosave] = useState(globalContext.autosave ?? { state: 'off' });

    // Changer l'état de l'indicateur
    const setAutosaveState = useCallback(state => {
        setAutosave({ state });
    }, []);

    return {
        key: 'autosave',
        state: autosave,
        operations: {
            setAutosaveState
        },
        reset: () => setAutosave({ state: 'off' })
    };
}
