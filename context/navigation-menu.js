/**
 * Contexte du menu d'application
 */

// React
import { useState, useCallback } from 'react';

// Contexte du menu de navigation
const initialState = {
    open: true
};

// Context factory
export function createNavigationMenuContext(globalContext) {
    const [navigationMenu, setNavigationMenu] = useState(globalContext.navigationMenu ?? initialState);

    // Ouverture et fermeture du menu
    function toggleNavigationMenu() {
        setNavigationMenu(navigationMenu => ({ ...navigationMenu, open: !navigationMenu.open }));
    }

    // Forcer l'ouverture
    function openNavigationMenu() {
        setNavigationMenu(navigationMenu => ({ ...navigationMenu, open: true }));
    }

    // Forcer la fermeture
    function closeNavigationMenu() {
        setNavigationMenu(navigationMenu => ({ ...navigationMenu, open: false }));
    }

    return {
        key: 'navigationMenu',
        state: navigationMenu,
        operations: {
            toggleNavigationMenu: useCallback(() => toggleNavigationMenu(), []),
            openNavigationMenu: useCallback(() => openNavigationMenu(), []),
            closeNavigationMenu: useCallback(() => closeNavigationMenu(), [])
        },
        reset: () => setNavigationMenu(initialState)
    };
}
