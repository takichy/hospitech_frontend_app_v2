/**
 * Contexte du timeout de session
 */

import { useState } from 'react';

export function createSessionTimeoutContext(globalContext) {
    const [sessionTimeout, setSessionTimeout] = useState(globalContext.sessionTimeout ?? false);

    return {
        key: 'sessionTimeout',
        state: sessionTimeout,
        operations: {
            setSessionTimeout
        },
        reset: () => setSessionTimeout(false)
    };
}
