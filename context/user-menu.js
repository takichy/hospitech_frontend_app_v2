/**
 * Contexte du menu utilisateur
 */

// React
import { useState, useCallback } from 'react';

// Service AppID
import { AppIDService } from '../services';

// Contexte
export function createUserMenuContext(globalContext) {
    const appidService = AppIDService.getInstance();
    const [userMenu, setUserMenu] = useState(
        globalContext.userMenu ?? { changePasswordDialogOpen: false, editProfileDialogOpen: false }
    );

    // Ouvrir le dialogue de changement de mot de passe
    const selectChangePassword = useCallback(() => {
        console.log('Ouverture du dialogue');
        setUserMenu(context => ({ ...context, changePasswordDialogOpen: true }));
    }, []);

    // Annuler le dialogue de changement de mot de passe
    const changePasswordDialogCancel = useCallback(() => {
        setUserMenu(context => ({ ...context, changePasswordDialogOpen: false }));
    }, []);

    // Confirmer le changement de mot de passe
    const changePasswordDialogConfirm = useCallback(async (user, password) => {
        setUserMenu(context => ({ ...context, changePasswordDialogOpen: false }));
        try {
            await appidService.changePassword(user.appId_id, password);
        } catch (error) {
            console.error('Erreur lors du changement de mot de passe:', error);
        }
    }, []);

    return {
        key: 'userMenu',
        state: userMenu,
        operations: {
            selectChangePassword,
            changePasswordDialogCancel,
            changePasswordDialogConfirm
        },
        reset: () => setUserMenu({ changePasswordDialogOpen: false, editProfileDialogOpen: false })
    };
}
