/**
 * Contexte de l'utilisateur loggé
 */

import { useState, useCallback } from 'react';

export function createUserContext(globalContext) {
    const [user, setUser] = useState(globalContext.user ?? {});

    return {
        key: 'user',
        state: user,
        operations: {
            setUser,
            updateProfile: useCallback(profile => setUser(user => ({ ...user, ...profile })), [user, setUser]),
            updateAvatar: picture => setUser(user => ({ ...user, picture, pictureTimestamp: Date.now() }))
        },
        reset: () => setUser({})
    };
}
