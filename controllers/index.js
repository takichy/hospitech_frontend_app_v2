/*
 * index.js
 * Point d'entrée du module des controleurs. Renvoi les controleurs enregistrés.
 */

export { userController } from './user/user-controller';
export { useProfileController } from './user/profile-controller';
export { useLicenceController } from './licence/licence-controller';
export { useSubscribeController } from './user/subscribe-controller';
