/**
 * Controleur de page Licence
 */

export function useLicenceController() {
    async function handleSubscriptOnPack(type) {
        console.log('handleSubscription: ', type);
    }

    return {
        handleSubscriptOnPack
    };
}
