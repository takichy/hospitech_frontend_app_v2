/**
 * Controleur des pages d'administration
 */

import { stockService } from '../../services/stocks';

// Page organizations
import makeUseStocksController from './stocks.controller';
const useStocksController = makeUseStocksController(stockService);

export { useStocksController };
