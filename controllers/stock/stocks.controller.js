/**
 * Controleur de la page Stocks
 */

import { useState, useEffect } from 'react';

// Factory du controller
export default function makeUseStocksController(stocksServiceManager) {
    return () => {
        const [stocks, setStocks] = useState(null);
        // const [selectedStock, setSelectedStock] = useState(null);

        // Chargement des stocks
        useEffect(() => {
            loadStocks();
        }, []);

        // Initialisation de la page
        const loadStocks = async () => {
            try {
                const rawStocks = await stocksServiceManager.getAllStocks();
                setStocks(rawStocks);
            } catch (error) {
                console.error('[stocks-controller] Erreur chargemement des stocks:', error);
            }
        };

        // Etat et indicateur de progression
        return {
            stocks
        };
    };
}
