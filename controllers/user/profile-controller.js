/**
 * Controleur de page Profile
 */

import { useEffect, useState } from 'react';

const axios = require('axios');
const credentials = require('../../config/env-proxy.json');

export function useProfileController() {
    const [pageState, setPageState] = useState({});

    // Chargement
    useEffect(async () => {
        await getCurrentUser();
    }, []);

    async function getCurrentUser() {
        const token = localStorage.getItem('token');

        const response = await axios.get(`${credentials.API_URL}users/profile`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });

        setPageState({
            profile: response.data.content.user
        });
    }

    async function handleProfileFormSubmit(dataForm) {
        console.log('handleProfileFormSubmit:: ', dataForm);
    }

    return {
        ...pageState,
        getCurrentUser,
        handleProfileFormSubmit
    };
}
