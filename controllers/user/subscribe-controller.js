/**
 * Définition du contrôleur de Subscribe
 *
 * Ce contrôleur contient la logique concernant les actions effectuées sur la page Subscribe.
 *
 */

// React
import { useRouter } from 'next/router';

const axios = require('axios');

const credentials = require('../../config/env-proxy.json');

// Controleur
function useSubscribeController() {
    const router = useRouter();

    const handleCreateNewUserWithAdminRole = async formData => {
        const userToRegister = {
            username: formData.firstname,
            email: formData.email,
            password: formData.password,
            firstname: formData.firstName,
            lastname: formData.lastName,
            role: 'ROLE_USER'
        };

        try {
            await axios.post(`${credentials.API_URL}users/register`, userToRegister);
            return router.push('/logon');
        } catch (error) {
            console.error("Erreur d'inscription de l'utilisateur:", error);
        }
    };

    return { handleCreateNewUserWithAdminRole };
}

export { useSubscribeController };
