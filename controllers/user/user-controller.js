/**
 * user-controller.js
 *
 * Définition de controller user
 * Ce controller va faire la gestion des utilisateurs
 */

const axios = require('axios');
const credentials = require('../../config/env-proxy.json');
import { useRouter } from 'next/router';

export default function userController() {
    const router = useRouter();

    return {
        /**
         *
         * @param {Object} formData objet de la formulaire login avec email et password
         * @returns
         */
        async handleLogin(formData) {
            return await axios
                .post(`${credentials.API_URL}users/login`, formData)
                .then(response => {
                    localStorage.setItem('token', response.data.content.token);
                    return router.push('/');
                })
                .catch(error => {
                    console.log('error: ', error);
                });
        }
    };
}
