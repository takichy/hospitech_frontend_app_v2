/**
 * Page d'erreur 404 personnalisée.
 * Evite les avertissements de react-i18next et l'avertissements dû à _error.js
 * Cf. https://err.sh/next.js/custom-error-no-custom-404
 */

import Error from 'next/error';

export default function Error404() {
    return <Error statusCode={404} />;
}
