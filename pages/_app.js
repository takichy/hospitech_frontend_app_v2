/**
 * Wrapper d'application Next.
 * @module pages/_app
 *
 * Utilisé pour injecter les mécanismes de traduction, de contexte applicatif et définir le theme général
 */

// Nom de l'application
const appTitle = require('../package').title;

// Général React
import { useEffect } from 'react';
import PropTypes from 'prop-types';

// Next
import Head from 'next/head';

// Theming
import CssBaseline from '@material-ui/core/CssBaseline';
import { createTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import theme from '../theme';

// Next
import { withRouter } from 'next/router';

/**
 * Définition de l'application de base
 * Inspiré de https://github.com/mui-org/material-ui/blob/master/examples/nextjs/pages/_app.js
 */
function BaseApp({ Component, pageProps }) {
    useEffect(() => {
        // Remove the server-side injected CSS.
        const jssStyles = document.querySelector('#jss-server-side');
        if (jssStyles) {
            jssStyles.parentElement.removeChild(jssStyles);
        }
    });

    return (
        <ThemeProvider theme={createTheme(theme)}>
            <Head>
                <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no" />
                <title>{appTitle}</title>
            </Head>
            <CssBaseline />
            <Component {...pageProps} />
        </ThemeProvider>
    );
}

BaseApp.propTypes = {
    Component: PropTypes.elementType.isRequired,
    pageProps: PropTypes.object.isRequired
};

export default withRouter(BaseApp);
