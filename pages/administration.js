// Composants applicatifs
import Layout from '../components/commons/Layout';

export default function Administration() {
    // Template général de la page
    return (
        <Layout title="Administration">
            <div>
                <h1>Administration</h1>
            </div>
        </Layout>
    );
}
