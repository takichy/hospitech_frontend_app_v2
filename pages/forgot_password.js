/**
 * Page de demande de reset du mot de passe
 */

// Material UI
import Box from '@material-ui/core/Box';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';

// Composants applicatifs
import DisconnectedLayout from '../components/commons/DisconnectedLayout';
import ForgotPasswordForm from '../components/Profile/ForgotPasswordForm';

// Controleur de la page
// import { useForgotPasswordController } from '../controllers';

// Composant
export default function Subscribe() {
    // const controller = useForgotPasswordController();

    let pageBody = null;
    // if (controller.error) {
    pageBody = (
        <Box textAlign="center" marginTop={6}>
            <Typography>Une erreur inattendue est survenue.</Typography>
            <Typography>Recommencer ou contacter l&apos;administrateur !</Typography>
        </Box>
    );
    // } else if (controller.queued) {
    pageBody = (
        <Box textAlign="center" marginTop={6}>
            <Typography>Votre demande d&apos;initialisation de mot de passe a été émise.</Typography>
            {/* {controller.emailMayNotBeSent ? ( */}
            <Typography>Néanmoins, il est possible que le mail n&apos;ait pas été envoyé.</Typography>
            {/* ) : ( */}
            <Typography>Surveillez votre boite mail et n&apos;oubliez pas le dossier des spams !</Typography>
            {/* )} */}

            <br />
            <Typography variant="caption">
                Si vous ne l&apos;avez pas reçu dans les prochaines minutes, recommencez ou contactez l&apos;administrateur pour
                vous renvoyer le mail.
            </Typography>
        </Box>
    );
    // } else if (controller.progress) {
    pageBody = (
        <Box textAlign="center" marginTop={6}>
            <CircularProgress color="primary" size={128} />
        </Box>
    );
    // } else {
    pageBody = (
        <Box textAlign="center" px={12} marginTop={6}>
            {/* {controller.unknownEmail ? ( */}
            <Typography>
                L&apos;email <strong>controller.unknownEmail</strong> est inconnu. Merci de fournir un email valide.
            </Typography>
            {/* ) : ( */}
            <Typography>Merci de sasir votre email.</Typography>
            {/* )} */}
            <br />
            <ForgotPasswordForm /*onSubmit={controller.validateForm}*/ />
            <br />
            <br />
            <br />
            <Typography variant="caption">
                Après avoir validé, vous allez recevoir un email vous invitant à renouveler votre mot de passe. Ce message contient
                un lien unique vers un formulaire qui vous permettra de sasir votre nouveau mot de passe.
            </Typography>
        </Box>
    );
    // }

    return (
        <DisconnectedLayout title="Réinitialisation mot de passe">
            <Box padding={3}>{pageBody}</Box>
        </DisconnectedLayout>
    );
}
