/**
 * Réimplémentation de la page d'accueil avec les hooks.
 */

// Vérification des types
import PropTypes from 'prop-types';

// Composant Material UI
import Grid from '@material-ui/core/Grid';

// Composants applicatifs
import Layout from '../components/commons/Layout';
import UserGreetings from '../components/index/UserGreetings';

// Composant
export default function Index({ loggedUser }) {
    // Template général de la page
    return (
        <Layout user={loggedUser} title="Accueil">
            <Grid container spacing={3}>
                <UserGreetings userName={loggedUser.lastName} />
            </Grid>
        </Layout>
    );
}

Index.propTypes = {
    loggedUser: PropTypes.shape({
        email: PropTypes.string.isRequired,
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        role: PropTypes.string.isRequired
    })
};

export const getServerSideProps = async context => ({
    props: {
        ...loggedUserProps(context.req)
    }
});

// La page Index est la première page affichée.
// On en profite pour récupérer les informations sur l'utilisateur que l'on passe au composant.
function loggedUserProps(req) {
    return {
        loggedUser: {
            email: 'req.user.email',
            firstName: 'req.user.firstName',
            lastName: 'req.user.lastName',
            role: 'req.user.Role'
        }
    };
}
