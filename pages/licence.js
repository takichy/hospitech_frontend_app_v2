// Composants applicatifs
import { Grid } from '@material-ui/core';
import Layout from '../components/commons/Layout';
import CardOffer from '../components/commons/Pricing/CardOffer';

// Controleur
import { useLicenceController } from '../controllers';

export default function Licence() {
    const controller = useLicenceController();

    const priceSubscriptionFormOne = 45;
    const priceSubscriptionFormTwo = 65;
    const priceSubscriptionFormThree = 100;

    const subscriptionFormOne = [
        { id: 0, name: 'Gestion des stock.' },
        { id: 1, name: 'Alerte en cas de pénurie.' },
        { id: 2, name: 'Alerte sur mobile et tablette.' },
        { id: 3, name: 'Commande automatique chez votre fournisseur (si le fournisseur le permet).' },
        { id: 4, name: 'Prediction des besoins en stock.' }
    ];

    const subscriptionFormTwo = [
        { id: 0, name: 'Gestion des arrivées.' },
        { id: 1, name: 'Alerte en temps sur le risque de saturation (trop plein) sur mobile et tablette.' },
        { id: 2, name: 'Redirection du trop plein sur les centres plus proches dans les alentours.' },
        { id: 3, name: 'Prediction des arrivées jusqu à 2 jours.' }
    ];

    const subscriptionFormThree = [
        { id: 0, name: 'Gestion des stock.' },
        { id: 1, name: 'Gestion des arrivées.' },
        { id: 2, name: 'Alerte en cas de pénurie.' },
        { id: 3, name: 'Alerte en temps sur le risque de saturation.' },
        { id: 4, name: 'Alerte sur mobile et tablette.' },
        { id: 5, name: 'Commande automatique chez votre fournisseur (si le fournisseur le permet).' },
        { id: 6, name: 'Prediction des besoins en stock.' }
    ];

    // Template général de la page
    return (
        <Layout title="Facturation">
            <Grid container direction="row" justifyContent="center" alignItems="center" spacing={1}>
                <Grid item>
                    <CardOffer
                        type={'Logistique'}
                        price={priceSubscriptionFormOne}
                        period={'mois'}
                        listOffers={subscriptionFormOne}
                        SubscriptOnPack={controller.handleSubscriptOnPack}
                    />
                </Grid>
                <Grid item>
                    <CardOffer
                        type={'Urgence'}
                        price={priceSubscriptionFormTwo}
                        period={'mois'}
                        listOffers={subscriptionFormTwo}
                        SubscriptOnPack={controller.handleSubscriptOnPack}
                    />
                </Grid>
                <Grid item>
                    <CardOffer
                        type={'Combo'}
                        price={priceSubscriptionFormThree}
                        period={'mois'}
                        listOffers={subscriptionFormThree}
                        SubscriptOnPack={controller.handleSubscriptOnPack}
                    />
                </Grid>
            </Grid>
        </Layout>
    );
}
