/**
 * Page de login
 */

// React
import PropTypes from 'prop-types';

// Material UI
import Alert from '@material-ui/lab/Alert';

// Formulaire de logon et autres composants
import DisconnectedLayout from '../components/commons/DisconnectedLayout';
import LogonForm from '../components/Logon/LogonForm';

// Composant
function Logon({ error }) {
    return (
        <DisconnectedLayout title="Se connecter">
            {error && <Alert variant="error" message={error} />}
            <LogonForm />
        </DisconnectedLayout>
    );
}

export const getServerSideProps = async ({ req }) => ({
    props: {
        ...getFlashMessage(req)
    }
});

// Capture du message flash si disponible
function getFlashMessage(req) {
    if (req?.flash) {
        const error = req.flash('error');
        if (error.length) {
            return { error: error[0] };
        }
    }
    return {};
}

// Validation
Logon.propTypes = {
    error: PropTypes.object
};

export default Logon;
