/**
 * Page d'édition du profile utilisateur
 */

// Composant Material UI
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';

// Composants applicatifs
import Layout from '../components/commons/Layout';
import ProfileForm from '../components/Profile/ProfileForm';

// Controleur
import { useProfileController } from '../controllers';

// Composant
export default function Profile() {
    const controller = useProfileController();
    // Template général de la page
    return (
        <Layout title="Mon Profile">
            <Box width="100%">
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <ProfileForm profile={controller.profile} onSubmit={controller.handleProfileFormSubmit} />
                    </Grid>
                </Grid>
            </Box>
        </Layout>
    );
}
