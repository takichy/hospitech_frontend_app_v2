/**
 * Pages de visualisation de l'ensemble des stocks
 */

import Grid from '@material-ui/core/Grid';

import Layout from '../components/commons/Layout';
import StockTable from '../components/Stocks/StockTable';

import { useStocksController } from '../controllers/stock';

// Page organisation
export default function stocks() {
    const { stocks } = useStocksController();

    return (
        <Layout title="Stocks">
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    {stocks && <StockTable stocks={stocks} />}
                </Grid>
            </Grid>
        </Layout>
    );
}
