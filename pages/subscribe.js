/**
 * Page de souscription
 */

// React
import PropTypes from 'prop-types';

// Material UI
import { Grid } from '@material-ui/core';
import { Alert } from '@material-ui/lab';

// Composants applicatifs
import DisconnectedLayout from '../components/commons/DisconnectedLayout';
import SubscribeForm from '../components/Profile/SubscribeForm';

// Controleur de la page
import { useSubscribeController } from '../controllers';

// Composant
export default function Subscribe({ error }) {
    const { handleCreateNewUserWithAdminRole } = useSubscribeController();

    return (
        <DisconnectedLayout title="S'enregistrer">
            {error && (
                <Alert severity="error" data-testid="alert">
                    {error}
                </Alert>
            )}
            <Grid container>
                <SubscribeForm onSubmit={handleCreateNewUserWithAdminRole} />
            </Grid>
        </DisconnectedLayout>
    );
}

// Validation
Subscribe.propTypes = {
    error: PropTypes.object
};
