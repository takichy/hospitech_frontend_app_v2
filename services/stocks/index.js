/**
 * Module client du service StockManager
 */

// Service StockService
import makeStocksServiceManager from './stocksServiceManager';
const stockService = makeStocksServiceManager();

export { stockService };
