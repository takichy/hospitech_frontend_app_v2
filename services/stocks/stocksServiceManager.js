/**
 * Module client du composant stockManager
 */

import axios from 'axios';

const credentials = require('../../config/env-proxy.json');

// Fonction factory
function makeStocksServiceManager() {
    return {
        // Renvoie toutes les stocks
        async getAllStocks() {
            const token = localStorage.getItem('token');

            const stocks = await axios.get(`${credentials.API_URL}stock`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });

            return stocks.data.list.stock;
        }
    };
}

export default makeStocksServiceManager;
