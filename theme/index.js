/*
 * index.js
 * Définition du thème de l'application
 */

// Import des palettes standars
import indigo from '@material-ui/core/colors/indigo';
import pink from '@material-ui/core/colors/pink';

export default {
    palette: {
        primary: {
            main: indigo['900']
        },
        secondary: {
            main: pink['400']
        }
    },
    typography: {
        useNextVariants: true
    },
    props: {
        MuiTextField: {
            variant: 'outlined'
        }
    },
    overrides: {
        MuiListItem: {
            root: {
                '&$selected': {
                    backgroundColor: indigo['100'],
                    color: indigo['900'],
                    '& div, & span': {
                        fontWeight: 'bold',
                        color: 'inherit'
                    }
                }
            }
        }
    }
};
